@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Projects</div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Priority</th>
                                <th>Author</th>
                                <th>Subject</th>
                                <th>Created_at</th>
                                <th>Options</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($project_all as $project)
                                <tr>
                                    <th scope="row">{{$project['id']}}</th>
                                    <td>{{$project['name']}}</td>
                                    <td>{{$project['status']}}</td>
                                    <td>{{$project['priority']}}</td>
                                    <td>{{$project['author']}}</td>
                                    <td>{{$project['subject']}}</td>
                                    <td>{{$project['created_at']}}</td>
                                    <td>
                                        <a href="/home/project/{{$project['id']}}">
                                            <button type="submit" class="btn btn-info" title="Reviews">Show</button>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="row">
                            <div class="col-sm-7">
                                <div class="dataTables_paginate paging_simple_numbers text-right"
                                     id="example1_paginate">
                                    <ul class="pagination">
                                        {{ $project_all->links() }}
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection