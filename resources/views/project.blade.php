@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <a href="/home">Back</a>
                <div class="card">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Priority</th>
                                <th>Author</th>
                                <th>Subject</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$project['id']}}</th>
                                    <td>{{$project['name']}}</td>
                                    <td>{{$project['status']}}</td>
                                    <td>{{$project['priority']}}</td>
                                    <td>{{$project['author']}}</td>
                                    <td>{{$project['subject']}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <h3>Comments</h3>
                @foreach($comments as $comment)
                <div class="card">
                    <div class="card-body">
                        <p>Author : {{$comment['user']['name']}}</p>
                        <p>Message : {{$comment['message']}}</p>
                    </div>
                </div><br>
                @endforeach
                <form method="POST" action="{{route('addComment')}}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input name="project_id" type="hidden" value="{{$project['id']}}"/>
                        <textarea name="message" cols="81" rows="5" placeholder="TEXT ....."></textarea>
                    <button type="submit" class="btn btn-success">Add Comment</button>
                </form>
            </div>
        </div>
    </div>
@endsection