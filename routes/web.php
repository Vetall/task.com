<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'MainController@showList');

Route::get('/save-data', 'MainController@saveIssues')->name('save-data');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/home/project/{id}', 'HomeController@show')->where(['id' => '[0-9]+'])->name('project');

// Create comment
Route::post('/addComment', 'HomeController@addComment')->name('addComment');


