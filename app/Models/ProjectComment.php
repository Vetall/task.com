<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectComment extends Model{
    protected $table = 'comments';

    protected $fillable = [
        'message',
        'user_id',
        'project_id',
        'title',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}