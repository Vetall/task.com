<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model{
    protected $table = 'project';
    public $timestamps = false;

    protected $fillable = [
        'name',
        'id',
        'status',
        'priority',
        'author',
        'subject',
        'created_at',
        'updated_at',
    ];
}