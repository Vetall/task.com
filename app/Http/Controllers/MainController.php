<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Services\RedmineIntegration;
use Carbon\Carbon;

class MainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Save Projects
     */
    public function saveIssues()
    {
        $projects =  (new RedmineIntegration())->issues();
        foreach ($projects['issues'] as $project){
            Project::query()->create([
                'id' => $project['id'],
                'name' => $project['project']['name'],
                'status' => $project['status']['name'],
                'priority' => $project['priority']['name'],
                'author' => $project['author']['name'],
                'subject' => $project['subject'],
                'created_at' => Carbon::parse($project['created_on']),
                'updated_at' => Carbon::parse($project['updated_on']),
            ]);
        }
    }

    public function showList()
    {
        return view('index');
    }
}