<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\ProjectComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show all list
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $project_all = Project::query()->paginate(6);

        return view('home', compact('project_all'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $project = Project::query()->where('id', $id)->firstOrFail();
        $comments = ProjectComment::query()->with('user')
            ->where('project_id', $id)
            ->orderBy('created_at', 'desc')
            ->paginate(50);
        return view('project', compact('project', 'comments'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addComment(Request $request)
    {
        $request->validate([
            'message' => 'required|min:10|max:3500',
        ]);

        ProjectComment::query()->create([
            'user_id' =>Auth::user()->id,
            'message' => $request['message'],
            'project_id' => $request['project_id'],
        ]);

        return redirect()->back()->with('message', 'SUCCESS');
    }
}