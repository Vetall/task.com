<?php

namespace App\Services;

use Redmine\Client;

class RedmineIntegration
{
    protected $client;

    public function __construct()
    {
        $client = new Client('https://redmine.ekreative.com', '2fda745bb4cdd835fdf41ec1fab82a13ddc1a54c');
        $client->user->all();
        $client->user->listing();

        $this->client = $client;
    }

    /**
     * Get projects
     * @return array
     */
    public function projects()
    {
        return $this->client->project->all([
            'limit' => 1000
        ]);
    }

    /**
     * Get Issue
     * @return array
     */
    public function issues()
    {
        return $this->client->issue->all([
            'limit' => 1000
        ]);
    }
}